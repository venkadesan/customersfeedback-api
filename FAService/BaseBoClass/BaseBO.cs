﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using Kowni.BusinessLogic;
using Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using System.Security.Cryptography;
using Kowni.Helper;
using KB = Kowni.BusinessLogic;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Web.Script.Serialization;
using System.Web.Script.Services;
using System.Text.RegularExpressions;
using System.IO;
using System.Text;



namespace FAService.BaseBoClass
{
    public class BaseBO
    {
        public void ClearHistory(System.Web.UI.Page aspxpage)
        {
            string strScript = "<script language=JavaScript> history.forward(); </script>";
            string strkey = "strkey1";
            if ((!aspxpage.ClientScript.IsStartupScriptRegistered(strkey)))
            {
                aspxpage.RegisterStartupScript(strkey, strScript);

            }
        }

        public void UpdateLog(string strdata)
        {
            UpdateLog(strdata, false);

        }

        public void UpdateLog(string strdata, bool Filter)
        {

            string path = HttpContext.Current.Server.MapPath("~/Log");
            if (!Directory.Exists(path))
                Directory.CreateDirectory(path);

            string IP = HttpContext.Current.Request.UserHostAddress;
            IP = IP.Replace(".", "");

            IP = IP.Replace(":", "");
            string filePath = string.Empty;
            //Creating object of SessionHelper Class  
           
            //setting session value in a variable  
            string sessionValue = "Logs";
            if (string.IsNullOrEmpty(sessionValue.ToString()) == false)
            {

                filePath = HttpContext.Current.Server.MapPath("~/Log/" + IP + "_" + sessionValue + "_event.log");

            }
            else
            {
                filePath = HttpContext.Current.Server.MapPath("~/Log/" + IP + "_event.log");
            }

            try
            {
                if (Filter == false)
                {
                    StreamWriter sw = System.IO.File.AppendText(filePath);
                    sw.WriteLine(DateTime.Now + " " + strdata);
                    sw.AutoFlush = true;
                    sw.Flush();
                    sw.Dispose();
                    sw.Close();
                }

            }
            catch (Exception ex)
            {
                Exception excep = new Exception("UpdateLog : " + ex.Message);
                throw excep;
            }
        }
    }
}