﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Services;
using System.Data;
using KB = Kowni.BusinessLogic;
using KCB = Kowni.Common.BusinessLogic;
using Microsoft.Practices.EnterpriseLibrary.Data;
using System.Data.Common;
using System.Configuration;
using System.Xml;
using System.Text;
using Kowni.Common.BusinessLogic;
using System.Web.Script.Serialization;
using System.IO;
using System.Web.Script.Services;
using FAService.BaseBoClass;

using System.Net.Mail;

using OfficeOpenXml;
using OfficeOpenXml.Drawing;
using OfficeOpenXml.Style;
using System.Drawing;
using OfficeOpenXml.Drawing.Chart;
using System.Globalization;
using System.Text.RegularExpressions;

namespace FAService
{
    /// <summary>
    /// Summary description for FacilityAudit
    /// </summary>
    [WebService(Namespace = "http://tempuri.org/")]
    [WebServiceBinding(ConformsTo = WsiProfiles.BasicProfile1_1)]
    [System.ComponentModel.ToolboxItem(false)]
    // To allow this Web Service to be called from script, using ASP.NET AJAX, uncomment the following line. 
    // [System.Web.Script.Services.ScriptService]
    public class FacilityAudit : System.Web.Services.WebService
    {

        KCB.BLSingleSignOn objSingleSignOn = new KCB.BLSingleSignOn();
        KCB.BLAnnouncement objAnnouncement = new KCB.BLAnnouncement();
        BaseBO objbasebo = new BaseBO();

        KB.BLUser.BLUserCompany objuserCompany = new KB.BLUser.BLUserCompany();
        KB.BLUser.BLUserLocation objuserLocation = new KB.BLUser.BLUserLocation();

        [WebMethod]
        public string HelloWorld()
        {
            //SendMail(Convert.ToDateTime("24-01-2017"), 1870, 5847, 20, 2, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, "0.0", "Kolk");
            //SendMail(Convert.ToDateTime("24-01-2017"), 1331, 4520, 20, 2, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, "0.0", "Kolk");

            //SendMail(auddate, companyid, locationid, sbuid, sectorid, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, "0.0", sbuname);
            //string trainingbody = File.ReadAllText(Server.MapPath("AppThemes/images/TrainingTemplate.txt"), Encoding.UTF8);
            //trainingbody = trainingbody.Replace("#location#", "E-Centric");

            //const String FROM = "noreply@ifazig.com";   // Replace with your "From" address. This address must be verified.
            //const String TO = "venkadesan.n@i2isoftwares.com";  // Replace with a "To" address. If your account is still in the
            //// sandbox, this address must be verified.

            //const String SUBJECT = "Test Mail from AWS";
            //const String BODY = "This email was sent through the Amazon SES SMTP interface by using C#.";

            //// Supply your SMTP credentials below. Note that your SMTP credentials are different from your AWS credentials.
            //const String SMTP_USERNAME = "AKIAJHHFMZSYI2UYE6IA";  // Replace with your SMTP username. 
            //const String SMTP_PASSWORD = "AoaYlTP6bNKS5jxS5LTs5Moggp46QIvF/KwcqFs2346i";  // Replace with your SMTP password.

            //// Amazon SES SMTP host name. This example uses the US West (Oregon) region.
            //const String HOST = "email-smtp.eu-west-1.amazonaws.com";

            //// The port you will connect to on the Amazon SES SMTP endpoint. We are choosing port 587 because we will use
            //// STARTTLS to encrypt the connection.
            //const int PORT = 587;

            //// Create an SMTP client with the specified host name and port.
            //using (System.Net.Mail.SmtpClient client = new System.Net.Mail.SmtpClient(HOST, PORT))
            //{
            //    // Create a network credential with your SMTP user name and password.
            //    client.Credentials = new System.Net.NetworkCredential(SMTP_USERNAME, SMTP_PASSWORD);

            //    // Use SSL when accessing Amazon SES. The SMTP session will begin on an unencrypted connection, and then 
            //    // the client will issue a STARTTLS command to upgrade to an encrypted connection using SSL.
            //    client.EnableSsl = true;

            //    // Send the email. 
            //    try
            //    {
            //        Console.WriteLine("Attempting to send an email through the Amazon SES SMTP interface...");
            //        client.Send(FROM, TO, SUBJECT, BODY);
            //        Console.WriteLine("Email sent!");
            //    }
            //    catch (Exception ex)
            //    {
            //        Console.WriteLine("The email was not sent.");
            //        Console.WriteLine("Error message: " + ex.Message);
            //    }
            //}

            return "Hello World";
        }

        [WebMethod]
        public string GetLoginDetails(string username, string password)
        {
            DataSet ds = new DataSet();
            DataTable table = new DataTable("LoginDetails");
            string message = string.Empty;
            //try
            //{
            int RoleID = 0;
            int UserID = 0;
            int CompanyID = 0;
            string CompanyName = string.Empty;
            string firstname = string.Empty;
            string lastname = string.Empty;
            string emailid = string.Empty;
            string mobileno = string.Empty;
            int LocationID = 0;
            string LocationName = string.Empty;
            int GroupID = 0;
            int LanguageID = 0;
            string UserFName = string.Empty;
            string UserLName = string.Empty;
            string UserMailID = string.Empty;
            string ThemeFolderPath = string.Empty;



            if (!objSingleSignOn.Login(username, password, Convert.ToInt32(ConfigurationManager.AppSettings["toolid"].ToString()), out RoleID, out UserID, out CompanyID, out CompanyName, out LocationID, out LocationName, out GroupID, out LanguageID, out UserFName, out UserLName, out UserMailID, out ThemeFolderPath))
            {
                message = "Invalid User !";
                table.Columns.Add("status");
                table.Rows.Add("0");
            }
            else
            {
                table.Columns.Add("status");
                table.Columns.Add("userID");
                table.Columns.Add("CompanyID");
                table.Columns.Add("locationID");
                table.Columns.Add("companyName");
                table.Columns.Add("locationame");
                table.Columns.Add("firstname");
                table.Columns.Add("lastname");
                table.Columns.Add("emailid");
                table.Columns.Add("mobileno");
                table.Rows.Add(1, UserID, CompanyID, LocationID, CompanyName, LocationName, UserFName, UserLName, UserMailID);

            }
            //}
            //catch { }

            return ConvertJavaSeriptSerializer(table);
        }


        [WebMethod]
        public string getallsector(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                int id = 0;
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallsectorbyuserid");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string Getcompanylogobyuserid(int companyid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (companyid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("getlogobycompanyid");
                    db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                    ds = db.ExecuteDataSet(cmd);
                }



            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }



        private string ConvertJavaSeriptSerializer(DataTable dt)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            List<object> resultMain = new List<object>();
            foreach (DataRow row in dt.Rows)
            {
                Dictionary<string, object> result = new Dictionary<string, object>();
                foreach (DataColumn column in dt.Columns)
                {
                    result.Add(column.ColumnName, "" + row[column.ColumnName]);
                }
                resultMain.Add(result);
            }
            return serializer.Serialize(resultMain);
        }

        [WebMethod]
        public string getallsbu(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_allsbu");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);

                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string GetCompanybyuserid(int userid)
        {
            DataSet ds = new DataSet();
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            try
            {
                if (userid != 0)
                {
                    //ds = objuserCompany.GetUserCompany(Convert.ToInt32(userid));

                    //DataTable dFiltered = ds.Tables[0].Clone();
                    //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    //foreach (DataRow dr in dRowUpdates)
                    //    dFiltered.ImportRow(dr);

                    //ds.Tables.Clear();
                    //ds.Tables.Add(dFiltered);
                    Database db = DatabaseFactory.CreateDatabase("DBNameIs");
                    DbCommand cmd = db.GetStoredProcCommand("sp_Getcompanybyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);
                    //DataTable dFiltered = ds.Tables[0].Clone();
                    //DataRow[] dRowUpdates = ds.Tables[0].Select(" UserCompanyID > 0", "companyname");
                    //foreach (DataRow dr in dRowUpdates)
                    //    dFiltered.ImportRow(dr);

                    //ds.Tables.Clear();
                    //ds.Tables.Add(dFiltered);

                }
            }
            catch { }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string GetLocationsbyuserid(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (userid != 0)
                {

                    Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                    DbCommand cmd = db.GetStoredProcCommand("And_getlocationbyuserid");
                    db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                    ds = db.ExecuteDataSet(cmd);


                }
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);
        }

        [WebMethod]
        public string getmaudit(int sectorid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getmaudit");
                db.AddInParameter(cmd, "sectorid", DbType.Int32, sectorid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }

        [WebMethod]
        public string getallaudit(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallmaudit");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }



        [WebMethod]
        public string getallcategory(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallcategory");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);


                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallquestion(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallquestion");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }


        [WebMethod]
        public string getallscore(int userid)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallscore");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }















        [WebMethod]
        public string gettransaction()
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {

                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("gettransaction");
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);


        }


        #region "After Auto Sync"


        [WebMethod]
        public string getallauditNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallmaudit_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallcategoryNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallcategory_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallquestionNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallquestion_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        [WebMethod]
        public string getallscoreNew(int userid, int type, string dtdate)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataSet ds = new DataSet();
            try
            {
                if (dtdate.Trim() == string.Empty)
                {
                    return serializer.Serialize("Invalid Date");
                }
                DateTime dt = DateTime.ParseExact(dtdate, "yyyy-MM-dd HH:mm:ss", CultureInfo.InvariantCulture);
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("And_getallscore_New");
                db.AddInParameter(cmd, "userid", DbType.Int32, userid);
                db.AddInParameter(cmd, "Type", DbType.Int32, type);
                db.AddInParameter(cmd, "Date", DbType.DateTime, dt);
                ds = db.ExecuteDataSet(cmd);
            }
            catch { return serializer.Serialize("No Data Found"); }
            return ConvertJavaSeriptSerializer(ds.Tables[0]);

        }

        #endregion



        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string InsertTransaction(int sbuid, int companyid, int locationid, int sectorid, string XMLIs, string clientname, string sitename,
                                        string ssano, string clientperson, string sbuname, string aomname, string auditorname, string auditeename,
                                       string deviceID, string guiD, int userid, string auditdate, string auditorsignature, string clientsignature, string observation, string feedback)
        {
            //XMLIs = "<root><facilityaudit  auditid='1' categoryid='1' questionid='1' scoreid='3' uploadfilename='' uploadfileGUID='6dc7bc84-1a2b-4f7d-8419-0f65c74d3af1' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='2' scoreid='3' uploadfilename='' uploadfileGUID='030960ae-b862-42bb-bca0-f7f2b0552aa5' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='3' scoreid='3' uploadfilename='' uploadfileGUID='11edd61b-71f7-4b2c-be57-fa6a356e944b' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='4' scoreid='3' uploadfilename='' uploadfileGUID='d043b863-be9b-4395-bd11-5a340d91c8a6' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='5' scoreid='3' uploadfilename='' uploadfileGUID='beccb295-7261-4f05-82ff-0ec32dddc84a' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='6' scoreid='3' uploadfilename='' uploadfileGUID='5421107c-f92f-426d-8529-18e91094502e' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='7' scoreid='3' uploadfilename='' uploadfileGUID='5db2196d-25ca-4d1d-884e-aa52d890e4f1' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='8' scoreid='3' uploadfilename='' uploadfileGUID='492a2255-a138-438f-a049-1509f085d7e8' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='9' scoreid='3' uploadfilename='' uploadfileGUID='964d9d08-efe8-42d8-b536-139eb1df711d' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='10' scoreid='3' uploadfilename='' uploadfileGUID='d3c5708b-0e18-482c-aed6-cbe1cd913121' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='11' scoreid='3' uploadfilename='' uploadfileGUID='db9f8e37-f28c-4c03-8a7c-2887cd6a2f69' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='12' scoreid='3' uploadfilename='' uploadfileGUID='d45b2e18-db87-49f2-8c23-caa98a7531f1' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='13' scoreid='3' uploadfilename='' uploadfileGUID='1d7337c5-1fcd-478f-ae71-45c51057cd6a' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='14' scoreid='3' uploadfilename='' uploadfileGUID='a50355c8-a9b1-425d-933e-d1e03c15301c' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='15' scoreid='3' uploadfilename='' uploadfileGUID='9279d3eb-1475-4cb5-b0d4-5bf0a2f04f24' remarks='' />,<facilityaudit  auditid='1' categoryid='1' questionid='16' scoreid='3' uploadfilename='' uploadfileGUID='80225c0d-6dae-4639-a096-b7d605b4ae40' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='18' scoreid='3' uploadfilename='' uploadfileGUID='2f43754d-de37-4dd6-b15c-3f8e8ae0c4b9' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='19' scoreid='3' uploadfilename='' uploadfileGUID='cb543597-6920-440c-9356-a087514c4a77' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='20' scoreid='3' uploadfilename='' uploadfileGUID='d75f8580-54c5-4c50-864e-0462f1253ea2' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='21' scoreid='3' uploadfilename='' uploadfileGUID='f8871250-6e87-48c1-be42-c65b950d7222' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='22' scoreid='3' uploadfilename='' uploadfileGUID='6b639322-44e1-4524-a847-2c870377db59' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='23' scoreid='3' uploadfilename='' uploadfileGUID='e63665e9-f4a4-40b5-9b99-dd36c24571da' remarks='' />,<facilityaudit  auditid='1' categoryid='2' questionid='24' scoreid='3' uploadfilename='' uploadfileGUID='138c0554-a34f-4a41-a45d-b8c754d693f2' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='25' scoreid='3' uploadfilename='' uploadfileGUID='4bea41fa-80bb-4ae8-b26f-65db2ff50737' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='26' scoreid='3' uploadfilename='' uploadfileGUID='4147aa9f-c2d5-44a5-b576-3ce5d3592960' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='27' scoreid='3' uploadfilename='' uploadfileGUID='34745d95-7404-4f34-ad3a-2b3f8a1b9f78' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='28' scoreid='3' uploadfilename='' uploadfileGUID='410456e2-3abf-4eb0-b7f7-20057f871119' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='29' scoreid='3' uploadfilename='' uploadfileGUID='980a2564-7edd-43ec-a72d-d6625bedf5b6' remarks='' />,<facilityaudit  auditid='1' categoryid='3' questionid='30' scoreid='3' uploadfilename='' uploadfileGUID='f81f30cf-885b-47d3-9afe-565fbf2ee1cc' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='31' scoreid='3' uploadfilename='' uploadfileGUID='cc7eb653-0936-43aa-86c0-bb56487f9f4f' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='32' scoreid='3' uploadfilename='' uploadfileGUID='7869161f-9690-488e-a83d-1cc38952f078' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='33' scoreid='3' uploadfilename='' uploadfileGUID='4f6feff7-feb7-4a42-acdc-3a52cf83fc5a' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='34' scoreid='3' uploadfilename='' uploadfileGUID='e98fce8a-f22a-45f2-b00f-6cd3aa18e819' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='35' scoreid='3' uploadfilename='' uploadfileGUID='462ab8f6-421d-4dd0-99db-d00a29e1a0df' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='36' scoreid='3' uploadfilename='' uploadfileGUID='f287222f-486f-46d4-839e-7641be65d654' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='37' scoreid='3' uploadfilename='' uploadfileGUID='9ccf70f4-636e-4d4a-9775-a464491c3602' remarks='' />,<facilityaudit  auditid='1' categoryid='4' questionid='38' scoreid='3' uploadfilename='' uploadfileGUID='74423a26-9d1e-47b3-8511-361c6610c3aa' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='39' scoreid='3' uploadfilename='' uploadfileGUID='313fbc59-3b6f-425e-ad69-4b1498e3c799' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='40' scoreid='3' uploadfilename='' uploadfileGUID='39c774bf-ab7f-49c1-b5dd-0b9217752db9' remarks='' />,<facilityaudit  auditid='1' categoryid='5' questionid='41' scoreid='3' uploadfilename='' uploadfileGUID='a15d4130-f473-4722-b0df-462db6a9f08d' remarks='' />,<facilityaudit  auditid='1' categoryid='6' questionid='42' scoreid='3' uploadfilename='' uploadfileGUID='bfe7a509-6992-4b2e-a04f-ccc37f1c5217' remarks='' />,<facilityaudit  auditid='1' categoryid='6' questionid='43' scoreid='3' uploadfilename='' uploadfileGUID='736f0099-18bc-4625-8557-da068b8a70db' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='44' scoreid='3' uploadfilename='' uploadfileGUID='15dbb7eb-b878-4573-b6f2-4d44b121d821' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='45' scoreid='3' uploadfilename='' uploadfileGUID='eb1dfddf-75c8-4c0c-a2f2-77232098d717' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='46' scoreid='3' uploadfilename='' uploadfileGUID='9abebbbd-0f11-466b-a9d4-3fcb9702a3e8' remarks='' />,<facilityaudit  auditid='1' categoryid='7' questionid='47' scoreid='3' uploadfilename='' uploadfileGUID='d74382fe-445b-477c-9196-8a7b344a620f' remarks='' />,<facilityaudit  auditid='1' categoryid='8' questionid='48' scoreid='3' uploadfilename='' uploadfileGUID='6695ea40-4a67-477a-bfe1-5533dd5dcfb6' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='49' scoreid='27' uploadfilename='' uploadfileGUID='4cb84fe9-9c52-4bfe-99ba-97aa042b2a53' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='50' scoreid='27' uploadfilename='' uploadfileGUID='bbb363b1-9d1a-4ae4-b5a1-6a2164c8bd2b' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='51' scoreid='27' uploadfilename='' uploadfileGUID='128f49df-4508-4b7c-afe3-61b5252035de' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='52' scoreid='27' uploadfilename='' uploadfileGUID='1c4b722a-abf8-46dc-bee9-8b3d5924010b' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='53' scoreid='27' uploadfilename='' uploadfileGUID='629e1d46-5ac7-48ec-be16-c20534b18ded' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='54' scoreid='27' uploadfilename='' uploadfileGUID='08e0a7d3-65d4-4c30-ab39-6b6773e85a92' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='55' scoreid='27' uploadfilename='' uploadfileGUID='80b7f1c2-1090-45f5-9fa5-78633cedf065' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='56' scoreid='27' uploadfilename='' uploadfileGUID='bcaee894-b079-49a0-8fb3-f5bbfd612ad8' remarks='' />,<facilityaudit  auditid='2' categoryid='9' questionid='57' scoreid='27' uploadfilename='' uploadfileGUID='85ba9d5d-de2f-4989-918d-1e06d5838b04' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='58' scoreid='27' uploadfilename='' uploadfileGUID='1a118db7-8b45-47c5-94cd-1607eb871aa7' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='59' scoreid='27' uploadfilename='' uploadfileGUID='5c685dc4-4040-449c-b738-2d3f032021c8' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='60' scoreid='27' uploadfilename='' uploadfileGUID='c995c19b-2690-4182-9d15-7026062fae0b' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='61' scoreid='27' uploadfilename='' uploadfileGUID='1cd7e919-edd4-49d9-aaae-9b368f8448de' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='62' scoreid='27' uploadfilename='' uploadfileGUID='0222e281-d52f-4a3e-b27c-5df4c24f20a1' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='63' scoreid='27' uploadfilename='' uploadfileGUID='93963e44-d222-4694-991f-f2a0f642fe65' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='64' scoreid='27' uploadfilename='' uploadfileGUID='13c9ed6f-3b9c-4de1-8242-4da048ec3b7f' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='65' scoreid='27' uploadfilename='' uploadfileGUID='4ef27e95-7af5-4cd1-af64-680785516bb7' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='66' scoreid='27' uploadfilename='' uploadfileGUID='c19c37a3-ac18-4854-bd35-93cfab599247' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='67' scoreid='27' uploadfilename='' uploadfileGUID='3144819a-681c-4659-a5e1-cad854c9a6e2' remarks='' />,<facilityaudit  auditid='2' categoryid='10' questionid='68' scoreid='27' uploadfilename='' uploadfileGUID='d9b1bd53-4475-4b10-bad4-4fdb50f64575' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='69' scoreid='27' uploadfilename='' uploadfileGUID='8e341e57-649a-4ece-8aff-0c1ab3cd727a' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='70' scoreid='27' uploadfilename='' uploadfileGUID='a1f7d82f-9343-40db-bc3e-60b64238d096' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='71' scoreid='27' uploadfilename='' uploadfileGUID='31584e98-02df-49f7-8ea5-29eefff19910' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='72' scoreid='27' uploadfilename='' uploadfileGUID='81012780-b18d-4a60-99c1-c1e8a98da32b' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='73' scoreid='27' uploadfilename='' uploadfileGUID='004b4369-6e4a-486e-a2af-0aaf12791b92' remarks='' />,<facilityaudit  auditid='2' categoryid='11' questionid='74' scoreid='27' uploadfilename='' uploadfileGUID='67d47213-73a6-41fb-9bb8-59858ee9fae5' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='75' scoreid='27' uploadfilename='' uploadfileGUID='a2dd4df8-3970-4623-9836-df99f8b1dea4' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='76' scoreid='27' uploadfilename='' uploadfileGUID='e2d9d6b5-8f3a-401f-bb98-367a9d2226fe' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='77' scoreid='27' uploadfilename='' uploadfileGUID='eaf3036e-a64e-42d3-81a5-f05193085d92' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='78' scoreid='27' uploadfilename='' uploadfileGUID='c9c136e4-2895-4f20-8818-69faf7850012' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='79' scoreid='27' uploadfilename='' uploadfileGUID='55f97d0f-e07d-4b1f-9c6b-8294b20866d4' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='80' scoreid='27' uploadfilename='' uploadfileGUID='3b809d4a-2396-412e-b09a-894a51c4f819' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='81' scoreid='27' uploadfilename='' uploadfileGUID='a4e4a484-83fd-4a75-9438-36695c8e98c6' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='82' scoreid='27' uploadfilename='' uploadfileGUID='edb1bc7f-22fe-4f07-ba3d-7b335222fa31' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='83' scoreid='27' uploadfilename='' uploadfileGUID='943b9526-56df-4dcf-999b-787c8ecbd764' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='84' scoreid='27' uploadfilename='' uploadfileGUID='f8f2a2b4-9c2c-414a-b3e6-7ddb5a42cbfd' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='85' scoreid='27' uploadfilename='' uploadfileGUID='c61ec509-a3b6-4d1f-89c0-837933404353' remarks='' />,<facilityaudit  auditid='2' categoryid='12' questionid='86' scoreid='27' uploadfilename='' uploadfileGUID='ee054d1e-ff4e-4337-b7cc-61251c00e0dd' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='87' scoreid='27' uploadfilename='' uploadfileGUID='b50f96fc-f109-4cb2-8d4e-2a116ecab0c1' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='88' scoreid='27' uploadfilename='' uploadfileGUID='461ca956-1b7e-4ffe-93e1-06b9494dec55' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='89' scoreid='27' uploadfilename='' uploadfileGUID='ff1bafa9-54da-48d2-8904-2c96569e048c' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='90' scoreid='27' uploadfilename='' uploadfileGUID='f73fb298-5fdb-4840-a402-32f1350beaf4' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='91' scoreid='27' uploadfilename='' uploadfileGUID='60f283af-68b3-4020-80c9-ac0537338360' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='92' scoreid='27' uploadfilename='' uploadfileGUID='ad686e53-2afb-4060-86a1-a1c1e7f3fccd' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='93' scoreid='27' uploadfilename='' uploadfileGUID='5d20d0e2-84b3-43c9-a71d-344bae5ca7ac' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='94' scoreid='27' uploadfilename='' uploadfileGUID='60de87a6-1bf3-4ddd-9597-ef1c77072565' remarks='' />,<facilityaudit  auditid='2' categoryid='13' questionid='95' scoreid='27' uploadfilename='' uploadfileGUID='f5155ce1-45c8-4bb1-bee6-6fc2b71745a3' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='96' scoreid='27' uploadfilename='' uploadfileGUID='2b12be88-0942-4571-a757-a0e80595a918' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='97' scoreid='27' uploadfilename='' uploadfileGUID='57ae8164-a034-4ad3-bd30-37ef8e586460' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='98' scoreid='27' uploadfilename='' uploadfileGUID='f8a94653-9904-4d41-af35-df395abe6257' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='99' scoreid='27' uploadfilename='' uploadfileGUID='c3fb989e-324b-4d0b-9962-808bf2c31498' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='100' scoreid='27' uploadfilename='' uploadfileGUID='36d73056-d971-445f-9cd9-8d4457f39b36' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='101' scoreid='27' uploadfilename='' uploadfileGUID='f575fa8b-701e-4d68-9904-580a7c4f1172' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='102' scoreid='27' uploadfilename='' uploadfileGUID='a27cff99-decc-4402-8d56-8aedfec98c80' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='103' scoreid='27' uploadfilename='' uploadfileGUID='c7432d43-3297-4208-b55e-e370c2a7dc28' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='104' scoreid='27' uploadfilename='' uploadfileGUID='0652f886-f0e2-4fa2-b8b8-c72f9054865b' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='105' scoreid='27' uploadfilename='' uploadfileGUID='6f74ea91-18cc-4f9a-852e-f0d587a8bace' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='106' scoreid='27' uploadfilename='' uploadfileGUID='333ec1d6-2caf-4774-ba37-4e90fb5abe79' remarks='' />,<facilityaudit  auditid='2' categoryid='14' questionid='107' scoreid='27' uploadfilename='' uploadfileGUID='238dc578-7cbf-4ecb-8de6-d060e1382943' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='108' scoreid='27' uploadfilename='' uploadfileGUID='d662050f-3189-4976-b632-35ee5db4b4fb' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='109' scoreid='27' uploadfilename='' uploadfileGUID='cf2e2f88-6591-419e-a768-ff93ad64c23f' remarks='' />,<facilityaudit  auditid='2' categoryid='15' questionid='110' scoreid='27' uploadfilename='' uploadfileGUID='8840e2cd-f12f-4e18-b9a0-997a5ee7b8fd' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='111' scoreid='27' uploadfilename='' uploadfileGUID='b0b69540-8e43-4759-bed7-569932b2bc93' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='112' scoreid='27' uploadfilename='' uploadfileGUID='2fc395d5-7ff0-4d40-b3d7-963918be55eb' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='113' scoreid='27' uploadfilename='' uploadfileGUID='131c1d89-bc5b-46c1-8bf6-5fa736fcce9d' remarks='' />,<facilityaudit  auditid='2' categoryid='16' questionid='114' scoreid='27' uploadfilename='' uploadfileGUID='6622e43d-0fe8-4a41-85c4-9dcb28d6b93d' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='115' scoreid='27' uploadfilename='' uploadfileGUID='38a4e87d-16a0-46ee-b148-aa934581ab25' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='116' scoreid='27' uploadfilename='' uploadfileGUID='c45c690e-7a54-4c7d-a8db-e6a4461d18bb' remarks='' />,<facilityaudit  auditid='2' categoryid='17' questionid='117' scoreid='27' uploadfilename='' uploadfileGUID='14244281-8826-4c8b-903a-057a052a3090' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='118' scoreid='27' uploadfilename='' uploadfileGUID='94bd31cc-0240-4686-b133-8be443a3aef2' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='119' scoreid='27' uploadfilename='' uploadfileGUID='fb3c3a48-d92d-4e27-b83d-4c80af5a1571' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='120' scoreid='27' uploadfilename='' uploadfileGUID='66e14646-193a-4c5f-add7-152b94f6bd74' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='121' scoreid='27' uploadfilename='' uploadfileGUID='dca65748-6a8d-4f14-8beb-80563eab5d02' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='122' scoreid='27' uploadfilename='' uploadfileGUID='eeeacb2e-39e3-4a5d-b1ff-ad1e0ad3956d' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='123' scoreid='27' uploadfilename='' uploadfileGUID='fc6d5966-f9a3-4ed7-aa0e-9ed4eb41d6e6' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='124' scoreid='27' uploadfilename='' uploadfileGUID='4bd5c4f8-48f5-4fff-b06a-c8d7ca62ba4c' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='125' scoreid='27' uploadfilename='' uploadfileGUID='65a3b8c3-c4ac-4cdd-a8f4-c4a159767ec2' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='126' scoreid='27' uploadfilename='' uploadfileGUID='eea545a6-9845-44ce-bc6a-8e371a57eb3e' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='127' scoreid='27' uploadfilename='' uploadfileGUID='747224f0-662c-480e-8ad6-d183b42a593c' remarks='' />,<facilityaudit  auditid='2' categoryid='18' questionid='128' scoreid='27' uploadfilename='' uploadfileGUID='1d143cd6-f983-4033-933a-d5e83232fcd1' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='129' scoreid='27' uploadfilename='' uploadfileGUID='31ab6842-e4cd-41b4-9ec4-4e30418c79c3' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='130' scoreid='27' uploadfilename='' uploadfileGUID='cb38f19a-ecc5-48a0-a993-83f826aadb49' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='131' scoreid='27' uploadfilename='' uploadfileGUID='b1046962-bf0b-45c1-8a73-e2d64f99746d' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='132' scoreid='27' uploadfilename='' uploadfileGUID='be5d8bbd-68fc-4d17-b2f2-14c58ecf47c6' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='133' scoreid='27' uploadfilename='' uploadfileGUID='aa40c520-7334-47b0-8bdd-c537f70668f2' remarks='' />,<facilityaudit  auditid='2' categoryid='19' questionid='134' scoreid='27' uploadfilename='' uploadfileGUID='b4258164-0466-4908-9ebe-00736b94664a' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='135' scoreid='27' uploadfilename='' uploadfileGUID='cb3b3a46-7461-4ca1-b86b-54e66bcd7a49' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='136' scoreid='27' uploadfilename='' uploadfileGUID='edce61fe-37ac-4a19-be4a-9c948b642921' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='137' scoreid='27' uploadfilename='' uploadfileGUID='3ed81388-ffc8-46d5-91f7-4a756a8473dc' remarks='' />,<facilityaudit  auditid='2' categoryid='20' questionid='138' scoreid='27' uploadfilename='' uploadfileGUID='88a51119-3e09-4bf6-8933-e20ba9312b84' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='140' scoreid='27' uploadfilename='' uploadfileGUID='579c804b-17b2-47bd-a107-c2d6fce2c3a8' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='141' scoreid='27' uploadfilename='' uploadfileGUID='b2e0f618-d37f-455b-bfcc-ead5bf394007' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='142' scoreid='27' uploadfilename='' uploadfileGUID='9e3f2043-bfe7-41b7-94e4-16b11acd012d' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='143' scoreid='27' uploadfilename='' uploadfileGUID='881a376d-00e6-41ec-aa8d-de7a1ba9fc53' remarks='' />,<facilityaudit  auditid='2' categoryid='21' questionid='144' scoreid='27' uploadfilename='' uploadfileGUID='25458234-fd52-49f6-ad51-f42a82a3cf86' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='145' scoreid='27' uploadfilename='' uploadfileGUID='a7f3ed48-c797-4918-89b4-dec910218c19' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='146' scoreid='27' uploadfilename='' uploadfileGUID='5b462737-258d-4702-a9ae-a8015bd72d0b' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='147' scoreid='27' uploadfilename='' uploadfileGUID='46503326-6033-49b4-8c83-e5d1ec4849fd' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='148' scoreid='27' uploadfilename='' uploadfileGUID='e79c455c-f40f-4bf1-aafc-3bcbc8e97ec4' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='149' scoreid='27' uploadfilename='' uploadfileGUID='a6f3daf0-ff2a-4633-80ee-23c575f1192e' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='150' scoreid='27' uploadfilename='' uploadfileGUID='5a5a603f-718a-4a6e-b118-37ae553916de' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='151' scoreid='27' uploadfilename='' uploadfileGUID='269bd7d5-4a90-4015-878f-4c2c0b03b454' remarks='' />,<facilityaudit  auditid='2' categoryid='22' questionid='152' scoreid='27' uploadfilename='' uploadfileGUID='f7915abd-63b0-46ad-9d21-aec874d0ec96' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='153' scoreid='27' uploadfilename='' uploadfileGUID='81678d8a-5187-4f97-b026-35a1d5fb6a46' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='154' scoreid='27' uploadfilename='' uploadfileGUID='09d0a9b9-647f-4c1c-9e8f-3e626486a55b' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='155' scoreid='27' uploadfilename='' uploadfileGUID='d91780a7-8cf2-45fa-b380-78ea419db97b' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='156' scoreid='27' uploadfilename='' uploadfileGUID='dbbff2f9-9724-4c0c-84a2-a28978dfa1d2' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='157' scoreid='27' uploadfilename='' uploadfileGUID='0f6430c3-0ae1-4257-9d63-7754d77926e5' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='158' scoreid='27' uploadfilename='' uploadfileGUID='3b8be8ea-68b7-4e90-ad6e-02ac14a11cf4' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='159' scoreid='27' uploadfilename='' uploadfileGUID='84660da4-9c1c-418c-8c5c-2108f393c2b5' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='160' scoreid='27' uploadfilename='' uploadfileGUID='e9940c50-408c-49b7-80b5-c87fb2577eb9' remarks='' />,<facilityaudit  auditid='2' categoryid='23' questionid='161' scoreid='27' uploadfilename='' uploadfileGUID='e743beaf-88b7-475b-b181-91bd36c34828' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='162' scoreid='27' uploadfilename='' uploadfileGUID='490e1472-bd83-4193-83ec-b0019081fe68' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='163' scoreid='27' uploadfilename='' uploadfileGUID='1211d1cc-4830-4b2c-859c-cdc958d249ae' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='164' scoreid='27' uploadfilename='' uploadfileGUID='44744b7f-728a-47da-939c-5e623c3e0817' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='165' scoreid='27' uploadfilename='' uploadfileGUID='b25b5001-3662-4646-9d0c-45836d554f01' remarks='' />,<facilityaudit  auditid='2' categoryid='24' questionid='166' scoreid='27' uploadfilename='' uploadfileGUID='8faf97e6-3bd8-412b-912e-c8bc59898890' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='167' scoreid='27' uploadfilename='' uploadfileGUID='e0323be7-ccf7-4e8c-9448-d164e33e4dd5' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='168' scoreid='27' uploadfilename='' uploadfileGUID='26c4cb86-68f4-42eb-b009-1781c4e7c8f2' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='169' scoreid='27' uploadfilename='' uploadfileGUID='d8ab371f-c9bf-45bc-9b2c-ad801cfaa873' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='170' scoreid='27' uploadfilename='' uploadfileGUID='497b1744-84a3-4f3b-83c7-75183bc15ee8' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='171' scoreid='27' uploadfilename='' uploadfileGUID='3ebad8fc-4b0e-4a87-9576-ea5716939d21' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='172' scoreid='27' uploadfilename='' uploadfileGUID='54afadbc-f0aa-4479-83e2-f3faea06441f' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='173' scoreid='27' uploadfilename='' uploadfileGUID='cb2ffe1c-24c0-4dd3-a6df-35a1e7ea413d' remarks='' />,<facilityaudit  auditid='2' categoryid='25' questionid='174' scoreid='27' uploadfilename='' uploadfileGUID='b39d0e61-d598-48d1-b494-ad26d3c8a4a3' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='175' scoreid='27' uploadfilename='' uploadfileGUID='d0e32cba-5697-4c24-98d1-206a188be1ca' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='176' scoreid='27' uploadfilename='' uploadfileGUID='9a7b72fb-258a-4960-a0c7-b15c3eb4cee3' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='177' scoreid='27' uploadfilename='' uploadfileGUID='61b50eb9-1340-4fbf-8b9d-e7a417df7fff' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='178' scoreid='27' uploadfilename='' uploadfileGUID='7e73a122-2f49-4c52-8885-7f0e9b0a71b1' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='179' scoreid='27' uploadfilename='' uploadfileGUID='b9665de1-6817-48a8-8b7a-7d84d87dc34d' remarks='' />,<facilityaudit  auditid='2' categoryid='26' questionid='180' scoreid='27' uploadfilename='' uploadfileGUID='c1eba97a-6114-4229-a125-d8f7ed16dab0' remarks='' /></root>";

            DataSet ds2 = new DataSet();
            Database db = DatabaseFactory.CreateDatabase("ConnectionString");
            DbCommand cmd = db.GetStoredProcCommand("CheckAduittransaction");
            db.AddInParameter(cmd, "deviceID", DbType.String, deviceID);
            db.AddInParameter(cmd, "guiD", DbType.String, guiD);
            ds2 = db.ExecuteDataSet(cmd);


            if (ds2 != null && ds2.Tables.Count > 0 && ds2.Tables[0].Rows.Count > 0)
            {
                return ConvertJavaSeriptSerializer(ds2.Tables[0]);
            }
            else
            {


                #region "New"

                try
                {

                    JavaScriptSerializer serializer = new JavaScriptSerializer();

                    DataSet dtinspect = new DataSet();
                    DataTable dlinspect = new DataTable();
                    StringBuilder objbuilder = new StringBuilder();




                    //"21:09:2015 13:54:12 PM"
                    //string[] fdate = XMLIs.Split(':');
                    //XMLIs = fdate[1].ToString() + "/" + fdate[0].ToString() + "/" + fdate[2].ToString() + ":" + fdate[3].ToString() + ":" + fdate[4].ToString();

                    byte[] auditorsign = null;
                    byte[] clientsign = null;
                    string auditorguid = "";
                    string clientguid = "";
                    try
                    {
                        string bas64Image = auditorsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            auditorsign = Convert.FromBase64String(bas64Image);
                            if (auditorsign.Length > 0)
                            {
                                auditorguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(auditorsign, ConfigurationManager.AppSettings["auditorsignature"] + auditorguid.Replace('/', ' '));
                                auditorguid = auditorguid + ".png";
                            }
                        }

                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }

                    try
                    {
                        string bas64Image = clientsignature;
                        if (bas64Image.Trim() != string.Empty)
                        {
                            clientsign = Convert.FromBase64String(bas64Image);
                            if (clientsign.Length > 0)
                            {
                                clientguid = Guid.NewGuid().ToString();
                                ByteArrayToImage(clientsign, ConfigurationManager.AppSettings["clientsignature"] + clientguid.Replace('/', ' '));
                                clientguid = clientguid + ".png";
                            }
                        }
                    }
                    catch (Exception e)
                    {
                        return serializer.Serialize(e.Message);
                    }

                    DateTime auddate = Convert.ToDateTime(auditdate);
                    DbCommand cmmd = db.GetStoredProcCommand("addmaudittransaction");
                    DataTable dtmsg = new DataTable();
                    dtmsg.Columns.Add("Status");
                    dtmsg.Columns.Add("Message");
                    dtmsg.Columns.Add("guid");
                    dtmsg.Columns.Add("deviceid");


                    db.AddInParameter(cmmd, "sbuid", DbType.Int32, sbuid);
                    db.AddInParameter(cmmd, "companyid", DbType.Int32, companyid);
                    db.AddInParameter(cmmd, "locationid", DbType.Int32, locationid);
                    db.AddInParameter(cmmd, "sectorid", DbType.Int32, sectorid);
                    db.AddInParameter(cmmd, "XMLIs", DbType.Xml, XMLIs);
                    db.AddInParameter(cmmd, "clientname", DbType.String, clientname);
                    db.AddInParameter(cmmd, "sitename", DbType.String, sitename);
                    db.AddInParameter(cmmd, "ssano", DbType.String, ssano);
                    db.AddInParameter(cmmd, "clientperson", DbType.String, clientperson);
                    db.AddInParameter(cmmd, "sbuname", DbType.String, sbuname);
                    db.AddInParameter(cmmd, "aomname", DbType.String, aomname);
                    db.AddInParameter(cmmd, "auditorname", DbType.String, auditorname);
                    db.AddInParameter(cmmd, "auditeename", DbType.String, auditeename);
                    db.AddInParameter(cmmd, "deviceID", DbType.String, deviceID);
                    db.AddInParameter(cmmd, "guiD", DbType.String, guiD);
                    db.AddInParameter(cmmd, "userid", DbType.Int32, userid);
                    db.AddInParameter(cmmd, "auddate", DbType.DateTime, auddate);
                    db.AddInParameter(cmmd, "auditorguid", DbType.String, auditorguid);
                    db.AddInParameter(cmmd, "clientguid", DbType.String, clientguid);
                    db.AddInParameter(cmmd, "observation", DbType.String, observation);
                    db.AddInParameter(cmmd, "feedback", DbType.String, feedback);


                    ds2 = db.ExecuteDataSet(cmmd);
                    dtmsg.Rows.Add("1", "Savedsucessfully", guiD, deviceID);

                    objbasebo.UpdateLog("Mail Function Start");
                    SendMail(auddate, companyid, locationid, sbuid, sectorid, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, string.Empty, 0, "0.0", sbuname, auditorname);
                    objbasebo.UpdateLog("Mail Function End");

                    return ConvertJavaSeriptSerializer(dtmsg);
                }
                catch (Exception e)
                {
                    objbasebo.UpdateLog("AddFeedBackDetails Error" + e.Message.ToString());
                    CreateErrorXML(XMLIs + " locationid:" + locationid.ToString());
                    JavaScriptSerializer serializer = new JavaScriptSerializer();
                    return serializer.Serialize(e.Message);
                }
                #endregion
            }

        }

        private void CreateErrorXML(string xml)
        {
            try
            {
                string filename = "XMLError" + DateTime.Now.ToString("yyyyMMdd");
                StreamWriter sw = File.AppendText(HttpContext.Current.Server.MapPath("~/Log/" + filename + "Log.log"));
                sw.WriteLine(DateTime.Now.ToString("yyyy-MM-dd hh:mm:ss") + " :: " + xml);
                sw.AutoFlush = true;
                sw.Flush();
                sw.Dispose();
                sw.Close();
            }
            catch
            {
            }
        }

        #region "Split Function"

        private string GetSiteID(string sitename)
        {
            string[] sitedet = sitename.Split('-');
            return sitedet[sitedet.Length - 1];
        }

        #endregion

        #region "Send Mail For ClientWise  MailIDs"
        private void SendMail(DateTime auditdate, int companyid, int locationid, int sbuid, int sectorid, string operations, string training, string scm, string compliance, string hr, string others, string followupdate, int closuredate, string totalscore, string sbuname, string auditorname)
        {
            try
            {
                DataSet dsRawdatas = GetRawDatasDateWiseReport(auditdate, locationid, sbuid, sectorid, auditorname);
                objbasebo.UpdateLog("Row Count : " + dsRawdatas.Tables[0].Rows.Count);
                if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                {
                    string path = System.AppDomain.CurrentDomain.BaseDirectory + "AuditFiles";
                    //string filename = path + "\\" + DateTime.Now.ToString("ddMMyyyyHHmmss") + ".xlsx";


                    string filename = Guid.NewGuid().ToString();
                    if (dsRawdatas.Tables[3].Rows.Count > 0)
                    {
                        DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                        filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty).Replace("*", string.Empty).Replace("?", string.Empty).Replace("\"", string.Empty);

                        RegexOptions options = RegexOptions.None;
                        Regex regex = new Regex("[ ]{2,}", options);
                        filename = regex.Replace(filename, " ");
                    }
                    string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                    string bodymail = File.ReadAllText(Server.MapPath("AppThemes/images/MailTemplate.txt"), Encoding.UTF8);
                    string subject = "Customer survey report of - #SbuName#";
                    string trainingmails = string.Empty;
                    string trainingsubject = "Training Score Alert- Client Feedback-#Location#";
                    string trainingbody = File.ReadAllText(Server.MapPath("AppThemes/images/TrainingTemplate.txt"), Encoding.UTF8);
                    if (dsRawdatas.Tables.Count > 4)
                    {
                        if (dsRawdatas.Tables[5].Rows.Count > 0)
                        {
                            DataRow drsubjectbody = dsRawdatas.Tables[5].Rows[0];
                            subject = drsubjectbody["mailsubject"].ToString();
                            bodymail = drsubjectbody["mailbody"].ToString();
                            trainingsubject = drsubjectbody["trainingsubject"].ToString();
                            trainingmails = drsubjectbody["trainingtomail"].ToString();
                            trainingbody = drsubjectbody["trainingbody"].ToString();
                            if (trainingmails.Trim() == string.Empty)
                            {
                                if (ConfigurationManager.AppSettings["trainingmails"] != null)
                                {
                                    trainingmails = ConfigurationManager.AppSettings["trainingmails"];
                                }                              
                            }
                            if (trainingsubject.Trim() == string.Empty)
                            {
                                if (ConfigurationManager.AppSettings["trainingsubject"] != null)
                                {
                                    trainingsubject = ConfigurationManager.AppSettings["trainingsubject"];
                                }
                            }
                        }
                    }

                    objbasebo.UpdateLog("Work Book Path :" + excelfilepath);
                    //WriteToFile(filename);

                    //GenerateExcel(dsRawdatas, totalscore, filename);

                    if (dsRawdatas.Tables[4].Rows.Count > 0)
                    {
                        DataRow drscore = dsRawdatas.Tables[4].Rows[0];
                        totalscore = drscore["score"].ToString();
                    }
                    DataTable dtTrainingDet = new DataTable();
                    dtTrainingDet = MakeExcel(dsRawdatas, totalscore, excelfilepath);

                    DataRow drHeader = dsRawdatas.Tables[3].Rows[0];
                    sbuname = drHeader["locsitename"].ToString();

                    //string bodymail = File.ReadAllText(Server.MapPath("AppThemes/images/MailTemplate.txt"), Encoding.UTF8);
                    bodymail = bodymail.Replace("#Operations#", operations);
                    bodymail = bodymail.Replace("#Training#", training);
                    bodymail = bodymail.Replace("#SCM#", scm);
                    bodymail = bodymail.Replace("#Compliance#", compliance);
                    bodymail = bodymail.Replace("#HR#", hr);
                    bodymail = bodymail.Replace("#Others#", others);
                    bodymail = bodymail.Replace("#Score#", totalscore + "%");
                    bodymail = bodymail.Replace("#Closure#", auditdate.AddDays(closuredate).ToShortDateString());
                    bodymail = bodymail.Replace("#SbuName#", sbuname);

                    subject = subject.Replace("#SbuName#", sbuname);

                    //string subject = "DTSS customer survey report of - " + sbuname;

                    if (followupdate.Trim() != string.Empty)
                    {
                        bodymail = bodymail.Replace("#Follow#", followupdate);
                    }
                    else
                    {
                        bodymail = bodymail.Replace("#Follow#", "Not Applicable");
                    }

                    SendingMail(bodymail, subject, excelfilepath, companyid, locationid, training, hr, scm, compliance, operations, auditdate, sbuid);

                    if (dtTrainingDet != null && dtTrainingDet.Rows.Count > 0)
                    {                      

                        trainingsubject = trainingsubject.Replace("#Location#", sbuname);
                        
                        trainingbody = trainingbody.Replace("#location#", sbuname);
                        trainingbody = trainingbody.Replace("#Content#", GenerateTemplate(dtTrainingDet));

                        SendTrainingMail(trainingbody, trainingsubject, excelfilepath, trainingmails, string.Empty);
                        objbasebo.UpdateLog("Training Mails for " + sbuname + " Send to" + trainingmails);
                    }

                    objbasebo.UpdateLog("Saved :" + path);
                }
            }
            catch (Exception)
            {
                throw;
            }
        }

        private void SendTrainingMail(string body, string subject, string attachment, string toMail, string ccMail)
        {
            try
            {

                string username = string.Empty;
                string password = string.Empty;
                string smtpServer = string.Empty;
                string frommail = string.Empty;
                int portno = 0;


                MailMessage mailMessage = new MailMessage();
                if (ConfigurationManager.AppSettings["UserName"] != null)
                {
                    username = ConfigurationManager.AppSettings["UserName"];
                }

                if (ConfigurationManager.AppSettings["Password"] != null)
                {
                    password = ConfigurationManager.AppSettings["Password"];
                }

                if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                {
                    smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                }

                if (ConfigurationManager.AppSettings["Port"] != null)
                {
                    portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                }
                if (ConfigurationManager.AppSettings["FromMail"] != null)
                {
                    frommail = Convert.ToString(ConfigurationManager.AppSettings["FromMail"]);
                }

                string newccmails = string.Empty;
                mailMessage.To.Add(toMail);
                if (ccMail.Trim() != string.Empty)
                {
                    string[] emailaddress = ccMail.Split(new[] { "," }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                    foreach (var address in emailaddress)
                    {
                        if (address != string.Empty)
                        {
                            newccmails = newccmails + address + ",";
                            mailMessage.CC.Add(address);
                        }
                    }
                }

                mailMessage.From = new MailAddress(frommail, "Client Feedback");
                mailMessage.Subject = subject;
                mailMessage.IsBodyHtml = true;
                mailMessage.Body = body;
                mailMessage.Attachments.Add(new Attachment(attachment));
                SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                smtpClient.UseDefaultCredentials = false;
                smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                smtpClient.EnableSsl = true;
                smtpClient.Send(mailMessage);
            }
            catch
            {
                throw;
            }
        }

        string GenerateTemplate(DataTable dtDatas)
        {
            try
            {
                string html = "<table rules='all' style='border-collapse:collapse' border='1' cellspacing='0'>";
                //add header row
                html += "<tr>";
                for (int i = 0; i < dtDatas.Columns.Count; i++)
                    html += "<td>" + dtDatas.Columns[i].ColumnName + "</td>";
                html += "</tr>";
                //add rows
                for (int i = 0; i < dtDatas.Rows.Count; i++)
                {
                    html += "<tr>";
                    for (int j = 0; j < dtDatas.Columns.Count; j++)
                        html += "<td>" + dtDatas.Rows[i][j].ToString() + "</td>";
                    html += "</tr>";
                }
                html += "</table>";
                return html;
            }
            catch
            {
                throw;
            }
        }


        private bool CheckNotApplicable(string crticalobservation)
        {
            try
            {
                bool bObservation = false;
                string[] stringArray = { "nil", "na", "n/a" };
                string value = crticalobservation.ToLower().Trim();
                int pos = Array.IndexOf(stringArray, value);
                if (pos > -1)
                {
                    bObservation = true;
                }
                return bObservation;
            }
            catch (Exception)
            {
                throw;
            }
        }


        private void SendingMail(string body, string subject, string attachment, int companyid, int locationid,
            string training, string hr, string scm, string compliance, string operations, DateTime Auditdate, int sbuid)
        {
            try
            {

                bool btraining = false, bhr = false, bscm = false, bcompliance = false, boperations = false;
                //btraining = CheckNotApplicable(training);
                //bhr = CheckNotApplicable(hr);
                //bscm = CheckNotApplicable(scm);
                //bcompliance = CheckNotApplicable(compliance);
                //boperations = CheckNotApplicable(operations);
                InsertMailTrackingDetails(Auditdate, companyid, locationid, sbuid, subject, body, attachment); // Insert mail details
                DataSet dsEmailDet = getlocationmailid(companyid, locationid, btraining, bhr, bscm, bcompliance, boperations);
                if (dsEmailDet.Tables.Count > 0 && dsEmailDet.Tables[0].Rows.Count > 0)
                {
                    DataRow drEmail = dsEmailDet.Tables[0].Rows[0];
                    string toMail = drEmail["ToMail"].ToString();
                    string CcMail = drEmail["CCMail"].ToString();

                    string username = string.Empty;
                    string password = string.Empty;
                    string smtpServer = string.Empty;
                    string frommail = string.Empty;
                    int portno = 0;


                    MailMessage mailMessage = new MailMessage();
                    if (ConfigurationManager.AppSettings["UserName"] != null)
                    {
                        username = ConfigurationManager.AppSettings["UserName"];
                    }

                    if (ConfigurationManager.AppSettings["Password"] != null)
                    {
                        password = ConfigurationManager.AppSettings["Password"];
                    }

                    if (ConfigurationManager.AppSettings["SmtpServer"] != null)
                    {
                        smtpServer = ConfigurationManager.AppSettings["SmtpServer"];
                    }

                    if (ConfigurationManager.AppSettings["Port"] != null)
                    {
                        portno = Convert.ToInt32(ConfigurationManager.AppSettings["Port"]);
                    }
                    if (ConfigurationManager.AppSettings["FromMail"] != null)
                    {
                        frommail = Convert.ToString(ConfigurationManager.AppSettings["FromMail"]);
                    }

                    //usermail = "venkadesan.n@i2isoftwares.com";
                    //ccmail="aravind@i2isoftwares.com";

                    //toMail = "stephenson.f@dtss.in";
                    //CcMail = "selvam.chokkappa@dtss.in,NHOperations@dtss.in";
                    if (toMail != string.Empty)
                    {
                        string newccmails = string.Empty;
                        foreach (var address in toMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries))
                        {
                            mailMessage.To.Add(address);
                        }
                        if (CcMail.Trim() != string.Empty)
                        {
                            string[] emailaddress = CcMail.Split(new[] { ",", ";" }, StringSplitOptions.RemoveEmptyEntries).Distinct().ToArray();
                            foreach (var address in emailaddress)
                            {
                                if (address != string.Empty)
                                {
                                    if (IsValidEmail(address.Trim()))
                                    {
                                        newccmails = newccmails + address + ",";
                                        mailMessage.CC.Add(address.Trim());
                                    }
                                }
                            }
                        }
                        newccmails = newccmails.TrimEnd(',');
                        UpdateMailIds(Auditdate, locationid, newccmails, toMail, false, string.Empty);

                        mailMessage.From = new MailAddress(frommail, "Client Feedback");
                        mailMessage.Bcc.Add(frommail);
                        mailMessage.Subject = subject;
                        mailMessage.IsBodyHtml = true;
                        mailMessage.Body = body;
                        mailMessage.Attachments.Add(new Attachment(attachment));
                        SmtpClient smtpClient = new SmtpClient(smtpServer, portno);
                        smtpClient.UseDefaultCredentials = false;
                        smtpClient.Credentials = new System.Net.NetworkCredential(username, password);
                        smtpClient.EnableSsl = true;
                        smtpClient.Send(mailMessage);

                        //FileInfo prevFile = new FileInfo(attachment);
                        //if (prevFile.Exists)
                        //{
                        //    prevFile.Delete();
                        //}
                        UpdateMessage(Auditdate, locationid, "Mail Sent Sucessfully.", true);
                        objbasebo.UpdateLog("Mail Sent To :" + toMail + " CC : " + CcMail);
                    }
                    else
                    {
                        UpdateMailIds(Auditdate, locationid, CcMail, toMail, false, "No Email ID(s) Found.");
                    }
                }
            }
            catch
            {
                throw;
            }
        }

        bool IsValidEmail(string email)
        {
            try
            {
                var addr = new System.Net.Mail.MailAddress(email);
                return addr.Address == email;
            }
            catch
            {
                return false;
            }
        }

        private DataTable MakeExcel(DataSet dsRawdatas, string Getscored, string excelfilepath)
        {
            DataTable dtTrainingDet = new DataTable();
            dtTrainingDet.Columns.Add("Sl.No");
            dtTrainingDet.Columns.Add("PARTICULARS");
            dtTrainingDet.Columns.Add("Maximum Score");
            dtTrainingDet.Columns.Add("Score Obtained");
            dtTrainingDet.Columns.Add("Remarks");
            int itraining = 1;
            try
            {

                //string filename = Guid.NewGuid().ToString();
                //if (dsRawdatas.Tables[3].Rows.Count > 0)
                //{
                //    DataRow drfilename = dsRawdatas.Tables[3].Rows[0];
                //    filename = drfilename["locsitename"].ToString().Replace(",", string.Empty).Replace("/", string.Empty).Replace(@"\", string.Empty).Replace("<", string.Empty).Replace(">", string.Empty);
                //}
                //string excelfilepath = Server.MapPath("DownloadExcels/") + filename + ".xlsx";

                FileInfo newFile = new FileInfo(excelfilepath);
                if (newFile.Exists)
                {
                    newFile.Delete();  // ensures we create a new workbook
                    newFile = new FileInfo(excelfilepath);
                }

                using (ExcelPackage package = new ExcelPackage(newFile))
                {
                    // add a new worksheet to the empty workbook
                    ExcelWorksheet worksheet = package.Workbook.Worksheets.Add("DownloadeExcel");

                    //Add the headers

                    StringBuilder sDatas = new StringBuilder();

                    float fEnteirAvg = 0;
                    int iTotalScore = 0;
                    int iTotalWeightage = 0;
                    int iMaxScore = 0;

                    DataTable dtChartdet = new DataTable();
                    dtChartdet.Columns.Add("Categoryname", typeof(string));
                    dtChartdet.Columns.Add("Value", typeof(float));
                    dtChartdet.Columns.Add("auditId", typeof(int));

                    if (dsRawdatas.Tables.Count > 0 && dsRawdatas.Tables[0].Rows.Count > 0)
                    {

                        DataTable dtAudit = dsRawdatas.Tables[0];
                        DataTable dtCategory = dsRawdatas.Tables[1];
                        DataTable dtRawData = dsRawdatas.Tables[2];
                        DataRow drHeader = dsRawdatas.Tables[3].Rows[0];

                        //worksheet.Cells[1, 1].Value = "DUSTERS TOTAL SOLUTIONS SERVICES PVT LTD";

                        worksheet.Cells[1, 1].Value = drHeader["GroupName"].ToString().ToUpper();

                        var headerFont = worksheet.Cells[1, 1].Style.Font;
                        headerFont.Bold = true;
                        headerFont.Size = 16;
                        worksheet.Cells[1, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[1, 1].Style.VerticalAlignment = ExcelVerticalAlignment.Center;

                        worksheet.Cells["A1:E4"].Merge = true;

                        string logofile = drHeader["UploadLogoName"].ToString();
                        if (logofile.Trim() != string.Empty)
                        {
                            string filepath = ConfigurationManager.AppSettings["companylogo"].ToString() + logofile;

                            FileInfo fileinfo = new FileInfo(filepath);
                            var picture = worksheet.Drawings.AddPicture("logo", fileinfo);
                            picture.SetSize(100, 50);
                            picture.SetPosition(1, 0, 1, 0);
                        }

                        worksheet.Cells[5, 1].Value = "CLIENT FEEDBACK FORM";
                        worksheet.Cells[5, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + 5 + ":E" + 5 + ""].Merge = true;
                        worksheet.Cells[5, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;



                        worksheet.Cells[6, 1].Value = "Name of the Client";
                        worksheet.Cells[6, 3].Value = drHeader["clientname"].ToString();
                        //worksheet.Cells[5, 6].Value = "";
                        worksheet.Cells["A6:B6"].Merge = true;
                        worksheet.Cells["C6:E6"].Merge = true;


                        worksheet.Cells[7, 1].Value = "Name of the Site";
                        worksheet.Cells[7, 3].Value = drHeader["locsitename"].ToString();
                        worksheet.Name = drHeader["locsitename"].ToString();
                        worksheet.Cells["A7:B7"].Merge = true;
                        worksheet.Cells["C7:E7"].Merge = true;


                        worksheet.Cells[8, 1].Value = "Site ID";
                        worksheet.Cells[8, 3].Value = GetSiteID(drHeader["locsitename"].ToString());
                        worksheet.Cells["A8:B8"].Merge = true;
                        worksheet.Cells["C8:E8"].Merge = true;

                        worksheet.Cells[9, 1].Value = "Name/EmailId";
                        worksheet.Cells[9, 3].Value = GetSiteID(drHeader["auditor"].ToString());
                        worksheet.Cells["A9:B9"].Merge = true;
                        worksheet.Cells["C9:E9"].Merge = true;

                        worksheet.Cells[10, 1].Value = "Date";
                        worksheet.Cells[10, 3].Value = GetSiteID(drHeader["dispauditdate"].ToString());
                        worksheet.Cells["A10:B10"].Merge = true;
                        worksheet.Cells["C10:E10"].Merge = true;



                        int imaxrowcount = 11;

                        for (int iauditcount = 0; iauditcount < dtAudit.Rows.Count; iauditcount++)
                        {
                            DataRow draudit = dtAudit.Rows[iauditcount];
                            int auditid = Convert.ToInt32(draudit["auditid"].ToString());
                            string auditname = draudit["auditname"].ToString();

                            worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;

                            worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                            worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            imaxrowcount++;

                            worksheet.Cells[imaxrowcount, 1].Value = "Sl.No";
                            worksheet.Cells[imaxrowcount, 2].Value = "PARTICULARS";
                            worksheet.Cells[imaxrowcount, 3].Value = "Maximum Score";
                            worksheet.Cells[imaxrowcount, 4].Value = "Score Obtained";
                            worksheet.Cells[imaxrowcount, 5].Value = "Remarks";

                            var range = worksheet.Cells[imaxrowcount, 1, imaxrowcount, 5];
                            range.Style.Fill.PatternType = ExcelFillStyle.Solid;
                            range.Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                            imaxrowcount++;

                            int iscore = 0;
                            int iweightage = 0;
                            int iSingleAvg = 0;
                            int iTotalAvg = 0;
                            int iAuditMax = 0;
                            DataRow[] drCategory = dtCategory.Select("auditid=" + auditid);
                            for (int icategroryCount = 0; icategroryCount < drCategory.Length; icategroryCount++)
                            {
                                int categoryid = Convert.ToInt32(drCategory[icategroryCount]["categoryid"].ToString());
                                string categoryname = drCategory[icategroryCount]["categoryname"].ToString();
                                bool isTraining = Convert.ToBoolean(drCategory[icategroryCount]["isTraining"].ToString());

                                worksheet.Cells[imaxrowcount, 1].Value = icategroryCount + 1;
                                worksheet.Cells[imaxrowcount, 2].Value = categoryname;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 2].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 1].Style.Font.Size = 14;

                                worksheet.Cells["B" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;

                                DataRow[] drRawDatas = dtRawData.Select("categoryid=" + categoryid);

                                int iCategoryScore = 0;
                                int iCategoryTotal = 0;
                                int iCatMax = 0;

                                for (int iRawDatacount = 0; iRawDatacount < drRawDatas.Length; iRawDatacount++)
                                {
                                    string question = drRawDatas[iRawDatacount]["auditqname"].ToString();
                                    string remarks = drRawDatas[iRawDatacount]["remarks"].ToString();
                                    int score = Convert.ToInt32(drRawDatas[iRawDatacount]["score"].ToString());
                                    int weightage = Convert.ToInt32(drRawDatas[iRawDatacount]["weightage"].ToString());
                                    string scorename = drRawDatas[iRawDatacount]["scorename"].ToString();
                                    string imgFile = drRawDatas[iRawDatacount]["uploadfilename"].ToString();
                                    string transactionid = drRawDatas[iRawDatacount]["TransactionId"].ToString();
                                    string filelink = string.Empty;
                                    if (imgFile.Trim() != string.Empty)
                                    {
                                        string filepath = "http://ifazility.com/facilityaudit/ImageLists.aspx";
                                        if (ConfigurationManager.AppSettings["Imaglists"] != null)
                                        {
                                            filepath = ConfigurationManager.AppSettings["Imaglists"].ToString();
                                        }
                                        filelink = filepath + "?TransactionId=" + transactionid;
                                    }

                                    int tScore = 0;
                                    int MaxScore = 0;

                                    //if (score == -1)
                                    //{
                                    //    score = 0;
                                    //    tScore = -1;
                                    //    weightage = 0;
                                    //    MaxScore = 0;
                                    //}
                                    //else
                                    //{
                                    //    MaxScore = 1;
                                    //}

                                    if (score == -1)
                                    {
                                        score = 0;
                                        weightage = 0;
                                        MaxScore = 0;
                                        tScore = -1;
                                    }
                                    else
                                    {
                                        MaxScore = 5;
                                    }

                                    iscore += score;
                                    iweightage += weightage;

                                    iCategoryScore += score;
                                    iCategoryTotal += weightage;

                                    iTotalScore += score;
                                    iTotalWeightage += weightage;

                                    iSingleAvg = score * weightage;
                                    iTotalAvg += iSingleAvg;

                                    iMaxScore += MaxScore;
                                    iCatMax += MaxScore;
                                    iAuditMax += MaxScore;

                                    string _score = string.Empty;
                                    string _maxscore = string.Empty;
                                    if (tScore == 0)
                                    {
                                        _score = " " + score;
                                        _maxscore = " " + MaxScore;
                                    }
                                    else
                                    {
                                        _score = scorename;
                                        _maxscore = scorename;
                                    }


                                    worksheet.Cells[imaxrowcount, 1].Value = iRawDatacount + 1;
                                    worksheet.Cells[imaxrowcount, 2].Value = question;

                                    if (question.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    }

                                    //worksheet.Cells[imaxrowcount, 2].Style.WrapText = true;
                                    //worksheet.Cells[imaxrowcount, 3].Value = _maxscore;

                                    int MaxRefId;
                                    int ScorerefID;

                                    bool isNumeric = int.TryParse(_maxscore.Trim(), out MaxRefId);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = MaxRefId;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 3].Value = _maxscore;
                                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    isNumeric = int.TryParse(_score.Trim(), out ScorerefID);

                                    if (isNumeric)
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = ScorerefID;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                                        if (isTraining)
                                        {
                                            if (ScorerefID < 3)
                                            {
                                                dtTrainingDet.Rows.Add(itraining, question, _maxscore, ScorerefID, remarks);
                                                itraining++;
                                            }
                                        }
                                    }
                                    else
                                    {
                                        worksheet.Cells[imaxrowcount, 4].Value = _score;
                                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                    }

                                    //worksheet.Cells[imaxrowcount, 4].Value = _score;
                                    worksheet.Cells[imaxrowcount, 5].Value = remarks;

                                    if (remarks.Length >= 75)
                                    {
                                        worksheet.Cells[imaxrowcount, 5].Style.WrapText = true;
                                    }

                                    worksheet.Cells[imaxrowcount, 6].Value = string.Empty;

                                    //if (imgFile.Trim() != string.Empty)
                                    //{
                                    //    var cell = worksheet.Cells[imaxrowcount, 7];
                                    //    cell.Hyperlink = new Uri(filelink);
                                    //    cell.Value = "ImageList";
                                    //}

                                    imaxrowcount++;
                                }

                                float _CateWisTot = 0;
                                if (iCategoryTotal != 0)
                                {
                                    _CateWisTot = ((float)(iCategoryScore / iCategoryTotal));
                                }
                                else
                                {
                                    _CateWisTot = 0;
                                }

                                dtChartdet.Rows.Add(categoryname, _CateWisTot, auditid);


                                string sCatNA = string.Empty;
                                string sCatMaxNA = string.Empty;
                                sCatNA = iCatMax.ToString();
                                sCatMaxNA = iCategoryScore.ToString();
                                if (iCatMax == 0)
                                {
                                    sCatNA = "N/A";
                                }
                                if (iCategoryScore == 0)
                                {
                                    sCatMaxNA = "N/A";
                                }

                                worksheet.Cells[imaxrowcount, 1].Value = "Sub Total " + categoryname;

                                int _catena;
                                int _catmaxna;
                                bool Numeric = int.TryParse(sCatNA.Trim(), out _catena);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = _catena;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                    worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));

                                Numeric = int.TryParse(sCatMaxNA.Trim(), out _catmaxna);

                                if (Numeric)
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = _catmaxna;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                else
                                {
                                    worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;
                                    worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                                }
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                                worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));



                                //worksheet.Cells[imaxrowcount, 3].Value = sCatNA;
                                //worksheet.Cells[imaxrowcount, 4].Value = sCatMaxNA;

                                worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                                worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                                worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                                imaxrowcount++;
                            }
                            float iScoredAvg = 0;
                            iScoredAvg = ((float)(iscore * 100 / iweightage));
                            fEnteirAvg += iScoredAvg;

                            //worksheet.Cells[imaxrowcount, 1].Value = auditname;
                            //worksheet.Cells[imaxrowcount, 3].Value = iscore;
                            //worksheet.Cells[imaxrowcount, 4].Value = iAuditMax;

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;

                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;

                            //worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                            //worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                            //worksheet.Cells[imaxrowcount, 4].Value = iScoredAvg + "%";

                            //worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                            //worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                            //worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                            //imaxrowcount++;
                        }
                        float tAvg = fEnteirAvg / (dsRawdatas.Tables[0].Rows.Count);

                        worksheet.Cells[imaxrowcount, 1].Value = "GRAND TOTAL SCORE";
                        worksheet.Cells[imaxrowcount, 3].Value = iMaxScore;
                        worksheet.Cells[imaxrowcount, 4].Value = iTotalScore;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 3].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        worksheet.Cells[imaxrowcount, 3].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FCB133"));
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;

                        decimal scoreGot = Convert.ToDecimal(Getscored);


                        worksheet.Cells[imaxrowcount, 1].Value = "Total Score";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = Getscored;
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        //string setColour = string.Empty;
                        //if (scoreGot >= 83)
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#00FF00"));
                        //}
                        //else
                        //{
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.PatternType = ExcelFillStyle.Solid;
                        //    worksheet.Cells[imaxrowcount, 4].Style.Fill.BackgroundColor.SetColor(System.Drawing.ColorTranslator.FromHtml("#FFBE00"));
                        //}
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = "Percentage (%)";
                        worksheet.Cells[imaxrowcount, 3].Value = string.Empty;
                        worksheet.Cells[imaxrowcount, 4].Value = ((decimal)(scoreGot * 100) / 5) + "%";
                        worksheet.Cells["A" + imaxrowcount + ":B" + imaxrowcount + ""].Merge = true;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.Font.Bold = true;
                        worksheet.Cells[imaxrowcount, 4].Style.HorizontalAlignment = ExcelHorizontalAlignment.Center;
                        imaxrowcount++;



                        string observations = drHeader["observation"].ToString();
                        string clientremarks = drHeader["feedback"].ToString();


                        worksheet.Cells[imaxrowcount, 1].Value = "Additional Feedback by Client";
                        worksheet.Cells[imaxrowcount, 1].Style.HorizontalAlignment = ExcelHorizontalAlignment.Left;
                        worksheet.Cells[imaxrowcount, 1].Style.Font.Bold = true;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[imaxrowcount, 1].Value = clientremarks;
                        worksheet.Cells["A" + imaxrowcount + ":E" + imaxrowcount + ""].Merge = true;
                        imaxrowcount++;


                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Top.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Right.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Left.Style = ExcelBorderStyle.Thin;
                        worksheet.Cells[1, 1, imaxrowcount - 1, 5].Style.Border.Bottom.Style = ExcelBorderStyle.Thin;

                        // worksheet.Cells.AutoFitColumns(0);  //Autofit columns for all cells
                        // worksheet.Cells.AutoFitColumns(1);
                        //// worksheet.Cells.AutoFitColumns(2);

                        //worksheet.Column(2).Width = 140;
                        //worksheet.Column(5).Width = 140;

                        worksheet.Column(1).Width = 5;
                        worksheet.Column(2).Width = 65;
                        worksheet.Column(3).Width = 15;
                        worksheet.Column(4).Width = 15;
                        worksheet.Column(5).Width = 65;

                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(5);

                        //worksheet.Cells.AutoFitColumns(1);
                        //worksheet.Cells.AutoFitColumns(2);
                        //worksheet.Cells.AutoFitColumns(3);
                        //worksheet.Cells.AutoFitColumns(4);
                        //worksheet.Cells.AutoFitColumns(5);
                        //worksheet.Cells.AutoFitColumns(6);
                        //worksheet.Cells.AutoFitColumns(7);

                        int c1;

                        imaxrowcount = 7;
                        ExcelWorksheet worksheetChart = package.Workbook.Worksheets.Add("CategoryChart");

                        string Alphabetic = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                        int startchar = 0;
                        int colspace = 0;
                        foreach (DataRow drAuditRow in dtAudit.Rows)
                        {
                            int auditid = Convert.ToInt32(drAuditRow["auditid"].ToString());
                            string auditname = drAuditRow["auditname"].ToString();
                            DataRow[] drAuditCategories = dtChartdet.Select("auditid=" + auditid);
                            if (drAuditCategories.Length > 0)
                            {
                                DataTable dtCategoryChart = drAuditCategories.CopyToDataTable();
                                int startvalue = imaxrowcount;

                                char Firstcol = Alphabetic[startchar];
                                char Secondcol = Alphabetic[startchar + 1];

                                foreach (DataRow drchartdet in dtCategoryChart.Rows)
                                {
                                    string categoryname = drchartdet["categoryname"].ToString();
                                    decimal score = Convert.ToDecimal(drchartdet["Value"].ToString());

                                    worksheetChart.Cells["" + Firstcol + imaxrowcount].Value = categoryname;
                                    worksheetChart.Cells["" + Secondcol + imaxrowcount].Value = score;
                                    imaxrowcount++;
                                }

                                var chart = worksheetChart.Drawings.AddChart("chart" + imaxrowcount, eChartType.ColumnClustered3D);
                                var series = chart.Series.Add("" + Secondcol + startvalue + ":" + Secondcol + imaxrowcount, "" + Firstcol + startvalue + ":" + Firstcol + imaxrowcount);
                                //series.HeaderAddress = new ExcelAddress("'Sheet1'!B" + (startvalue - 1));
                                chart.SetSize(510, 300);
                                chart.Title.Text = auditname;
                                chart.SetPosition(7, 0, colspace, 0);
                                colspace += 9;

                                startchar += 2;
                            }
                        }


                        package.Save();
                    }

                }
                return dtTrainingDet;
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }

        #endregion

        #region "Get Mail Datatable"

        public DataSet GetRawDatasDateWiseReport(DateTime auditdate, int locationid, int sbuid, int sectorid, string auditorname)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetRawDatasDateWiseReport");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "SectorId", DbType.Int32, sectorid);
                db.AddInParameter(cmd, "Auditor", DbType.String, auditorname);
                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }

        public void InsertMailTrackingDetails(DateTime auditdate, int companyid, int locationid, int sbuid, string subject, string body, string attachments)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_InsertMailingSystem");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "sbuid", DbType.Int32, sbuid);
                db.AddInParameter(cmd, "Subject", DbType.String, subject);
                db.AddInParameter(cmd, "Body", DbType.String, body);
                db.AddInParameter(cmd, "Attachments", DbType.String, attachments);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMailIds(DateTime auditdate, int locationid, string ccmail, string tomail, bool isSent, string message)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMailIDs");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "ccmail", DbType.String, ccmail);
                db.AddInParameter(cmd, "ToMail", DbType.String, tomail);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public void UpdateMessage(DateTime auditdate, int locationid, string message, bool isSent)
        {
            try
            {
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Sp_UpdateMessageDet");
                db.AddInParameter(cmd, "AuditDate", DbType.DateTime, auditdate);
                db.AddInParameter(cmd, "locationId", DbType.Int32, locationid);
                db.AddInParameter(cmd, "Message", DbType.String, message);
                db.AddInParameter(cmd, "isSent", DbType.Boolean, isSent);
                db.ExecuteNonQuery(cmd);
            }
            catch
            {
                throw;
            }
        }

        public DataSet getlocationmailid(int companyid, int locationid, bool training, bool hr, bool scm, bool compliance, bool operations)
        {
            try
            {
                DataSet ds = new DataSet();
                Database db = DatabaseFactory.CreateDatabase("ConnectionString");
                DbCommand cmd = db.GetStoredProcCommand("Qry_GetLocationwiseMailIDs");
                db.AddInParameter(cmd, "companyid", DbType.Int32, companyid);
                db.AddInParameter(cmd, "locationid", DbType.Int32, locationid);

                db.AddInParameter(cmd, "bTraining", DbType.Boolean, training);
                db.AddInParameter(cmd, "bhr", DbType.Boolean, hr);
                db.AddInParameter(cmd, "bscm", DbType.Boolean, scm);
                db.AddInParameter(cmd, "bcompliance", DbType.Boolean, compliance);
                db.AddInParameter(cmd, "boperations", DbType.Boolean, operations);

                ds = db.ExecuteDataSet(cmd);
                return ds;
            }
            catch
            {
                throw;
            }
        }


        #endregion

        private void ByteArrayToImage(byte[] byteArrayIn, string Path)
        {
            //System.Drawing.Image.GetThumbnailImageAbort myCallBack = new System.Drawing.Image.GetThumbnailImageAbort(ThumbnailCallback);
            System.Drawing.Image newImage;


            if (byteArrayIn != null)
            {
                using (MemoryStream stream = new MemoryStream(byteArrayIn))
                {
                    //Bitmap newBitmap = new Bitmap(varBmp);
                    //varBmp.Dispose();
                    //varBmp = null;

                    newImage = System.Drawing.Image.FromStream(stream);

                    //var fileName = Path.GetFileName(fileurl.FileName);
                    //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                    newImage.Save(Path + "." + System.Drawing.Imaging.ImageFormat.Png);
                    newImage.Dispose();
                    newImage = null;


                }
            }
        }

        [WebMethod(EnableSession = true)]
        [ScriptMethod(ResponseFormat = ResponseFormat.Json)]
        public string uploadimages(string filename, string bas64Image, string guid, string deviceid)
        {
            byte[] data = null;
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            DataTable dtmsg = new DataTable();
            dtmsg.Columns.Add("Status");
            dtmsg.Columns.Add("Message");
            dtmsg.Columns.Add("guid");
            dtmsg.Columns.Add("deviceid");
            try
            {
                objbasebo.UpdateLog("AddFeedBackDetails Start");


                // bas64Image = images[j].TrimStart('"').TrimEnd('"');
                bas64Image = bas64Image.TrimStart('"').TrimEnd('"');
                data = Convert.FromBase64String(bas64Image);


                if (data.Length > 0)
                {

                    System.Drawing.Image newImage;
                    filename = filename.TrimEnd(',').Replace('&', ' ').Replace('/', ' ');

                    if (data != null)
                    {
                        using (MemoryStream stream = new MemoryStream(data))
                        {
                            //Bitmap newBitmap = new Bitmap(varBmp);
                            //varBmp.Dispose();
                            //varBmp = null;

                            newImage = System.Drawing.Image.FromStream(stream);

                            //var fileName = Path.GetFileName(fileurl.FileName);
                            //fileurl.SaveAs(Path.Combine(@"c:\projects", fileName));

                            newImage.Save(ConfigurationManager.AppSettings["Auditimages"] + filename);
                            newImage.Dispose();
                            newImage = null;
                            dtmsg.Rows.Add("1", "Saved Sucessfully", guid, deviceid);

                            return ConvertJavaSeriptSerializer(dtmsg);


                        }
                    }
                    //ByteArrayToImage(data, ConfigurationManager.AppSettings["Feedbackimage"] + filename);
                }
            }

            catch
            {
                dtmsg.Rows.Add("0", "Not Saved Sucessfully", guid, deviceid);
                return ConvertJavaSeriptSerializer(dtmsg);
            }

            dtmsg.Rows.Add("2", "Un defined erro", guid, deviceid);
            return ConvertJavaSeriptSerializer(dtmsg);
        }





    }
}